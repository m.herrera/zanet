
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from .models import Usuario
from .forms import UserCreationForm, UserChangeForm

class UsuarioAdmin(UserAdmin):
    """ usuario administrativo"""
    fieldsets = [
        (None, {"fields": ["email", "password"]}),
        ("Personal info", {"fields": ["first_name", "last_name"]}),
        ("Permissions", {"fields": ["is_active", "is_staff", "is_superuser", "groups", "user_permissions"]}),
        ("Important dates", {"fields": ["last_login", "date_joined"]}),
    ]
    
    add_fieldsets = [
        (
            None,
            {
                "classes": ["wide"],
                "fields": ["email", "password1", "password2"],
            },
        ),
    ]
    
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm
    
    list_display = ["email", "first_name", "last_name", "is_staff", "is_superuser"]
    search_fields = ["email", "first_name", "last_name"]
    ordering = ["email"]

# Register your models here.
admin.site.register(Usuario, UsuarioAdmin)
