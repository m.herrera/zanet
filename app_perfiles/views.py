from django.shortcuts import render, redirect
from django.views.generic import *
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Group, Permission
from bases.views import Sin_Privilegios
from django.contrib.auth.hashers import make_password
from django.urls import reverse_lazy
from django.db.models import Q
from django.http import HttpResponse

from .models import Usuario
from .forms import UserForm

# Create your views here.
class UserList(Sin_Privilegios, ListView):
    """ vbc para listar usuarios"""
    permission_required = "app_perfiles:view_usuario"
    model = Usuario
    template_name = "app_perfiles/users_list.html"
    login_url = "bases:login"
    context_object_name = "obj"
    
@login_required(login_url='/login/') # debe estar logeado 
@permission_required('app_perfiles.add_usuario', login_url='bases:sin_privilegios') 
@permission_required('app_perfiles.change_usuario', login_url='bases:sin_privilegios') 
def user_admin(request, pk=None):
    """ vista basada en función para crear/editar un usuario"""        
    template_name = "app_perfiles/users_add.html"
    context = {}    
    form = None
    obj = None
    
    if request.method == "GET":
        if not pk:
            form = UserForm(instance = None) # form vacío
        else:
            obj = Usuario.objects.filter(id=pk).first()
            form = UserForm(instance = obj)
        
        context["form"] = form
        context["obj"] = obj
    
    # capturamos los datos que vienen del form para enviarlos y/o guardarlos en DB
    if request.method == "POST":
        data = request.POST
        # capturamos los datos que vienen del form
        e = data.get("email")
        fn = data.get("first_name")
        ln = data.get("last_name")
        p = data.get("password")
        
        # validamos si el email ya existe        
        if pk:
            obj = Usuario.objects.filter(id=pk).first()
            if not obj:
                print("Error, usuario no existe")
            else:
                # creamos el usuario con los datos del form
                obj.email = e
                obj.first_name = fn
                obj.last_name = ln 
                obj.password = make_password(p) # enchiptamos el password con el hash
                obj.save()  
        
        else:
            obj = Usuario.objects.create_user(
                email = e, 
                password = p,
                first_name = fn,
                last_name = ln                
            )  
            
            print(obj.email, obj.password)
        
        return redirect('app_perfiles:users_list')
        
    return render(request, template_name, context)

class UserDelete(Sin_Privilegios, DeleteView):
    """ vista basada en clase para eliminar un usuario"""
    permission_required = "app_perfiles.delete_usuario"
    model = Usuario
    template_name = "app_perfiles/users_del.html"
    context_object_name = "obj"
    #success_message = "Localidad eliminada satisfactoriamente.."
    success_url = reverse_lazy("app_perfiles:users_list")
    
class UserGroupList(Sin_Privilegios, ListView):
    """ vbc para listar grupos de usuarios"""
    permission_required = "app_perfiles:view_usuario"
    model = Group
    template_name = "app_perfiles/user_group_list.html"
    login_url = "app_perfiles:login"
    context_object_name = "obj"
    
@login_required(login_url='/login/')
@permission_required('app_perfiles:change_usuario', login_url='bases:sin_privilegios')
def user_groups_admin(request, pk=None):
    """ permisos para un grupo"""
    template_name = "app_perfiles/users_group_add.html"
    context = {}
    
    # si existe el grupo
    obj = Group.objects.filter(id = pk).first()
    context['obj'] = obj
    permisos = {}
    permisos_grupo = {}
    context['permisos'] = permisos
    context['permisos_grupo'] = permisos_grupo
    
    if obj:
        # obtenemos los permisos de grupo
        permisos_grupo = obj.permissions.all()
        context['permisos_grupo'] = permisos_grupo
        
        # consultamos todos los permisos, exceptuando los que ya tiene el grupo
        permisos = Permission.objects.filter(~Q(group=obj))
        context["permisos"] = permisos
        
    # print(permisos)
    # print(permisos_grupo)
    
    if request.method == "POST":
        name = request.POST.get("name")
        grp = Group.objects.filter(name=name).first()
        
        if grp and grp.id != pk:
            print("Grupo ya existe, no puede volver a crearlo")
            return redirect("perfiles:users_groups_new")
        
        if not grp and pk != None:
            # Grupo existe, se está cambiando el nombre
            grp = Group.objects.filter(id=pk).first()
            grp.name = name
            
        elif not grp and pk == None: 
            grp = Group(name=name)
        else:
            ...
            
        grp.save()
        return redirect("perfiles:users_groups_modify", grp.id)
                
    return render(request, template_name, context)

@login_required(login_url='/login/')
@permission_required('app_perfiles:change_usuario', login_url='bases:sin_privilegios')
def user_group_delete(request, pk):
    """ vbf para borra grupo"""
    if request.method == "POST":
        grp = Group.objects.filter(id=pk).first()
        
        if not grp:
            print("Grupo no existe")
        else:
            grp.delete()
        
        return HttpResponse("OK")
    
