
from django.utils import timezone
from django.contrib.auth.models import BaseUserManager

class UsuarioManager(BaseUserManager):

    # método para crear usuario común y superusuario
    def _create_user(self, email, password, is_staff, is_superuser, **extra_fields):
        now = timezone.now()
        if not email:
            raise ValueError("Debe especificar email.")
        
        email = self.normalize_email(email)
        
        # creamos un usuario
        user = self.model(
            email = email,
            is_staff = is_staff,
            is_active = True,
            is_superuser = is_superuser,
            last_login = now,
            **extra_fields
        )
        
        # le asignamos contraseña
        user.set_password(password)
        user.save(using=self._db)
        
        return user
    
    # crea un usuario normal
    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False, **extra_fields)
    
    # crea un superusuario
    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True, **extra_fields)
        
