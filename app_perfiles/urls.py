
from django.urls import path
from django.contrib.auth import views as auth_views
from app_perfiles.views import *

app_name = 'perfiles'

urlpatterns = [
    # CRUD de usuarios
    path('usuarios/users-list/', UserList.as_view(), name="users_list"),
    path('usuarios/users-add/', user_admin, name="user_add"),
    path('usuarios/users-modify/<int:pk>', user_admin, name="user_modify"),
    path('usuarios/users-del/<int:pk>', UserDelete.as_view(), name="user_del"),
    
    # CRUD de grupos
    path('users/groups/list', UserGroupList.as_view(), name="users_groups_list"),
    path('users/groups/add', user_groups_admin, name="users_groups_new"),
    path('users/groups/modify/<int:pk>', user_groups_admin, name="users_groups_modify"),
    path('users/groups/delete/<int:pk>', user_group_delete, name="user_group_delete"),
]