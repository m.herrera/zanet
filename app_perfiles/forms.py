
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import Usuario
from django.forms import fields
from django.db import models
from django import forms
from django.forms.widgets import PasswordInput

class UsuarioCreationForm(UserCreationForm):
    """ form para crear un usuario solo con email y password"""
    
    def __init__(self, *args, **kwargs):
        super(UsuarioCreationForm, self).__init__(*args, **kwargs)
        
    class Meta:
        model = Usuario
        fields = ('email',)
    
class UsuarioChangeForm(UserChangeForm):
    """ form para editar un usuario"""
    
    def __init__(self, *args, **kwargs):
        super(UsuarioChangeForm, self).__init__(*args, **kwargs)
        
    class Meta:
        model = Usuario
        fields = '__all__'
        
class UserForm(forms.ModelForm):
    """ form para crear usuario"""
    password = forms.CharField(widget=PasswordInput)
    
    class Meta:
        model = Usuario
        fields = ['email', 'first_name', 'last_name', 'password']
        widget = {'email': forms.EmailInput, 'password': forms.PasswordInput}
    
    # a todos los elementos del form le ponemos la clase form-control
    def __init__(self, *args, **kwargs):
        """ método que itera los campos del form y agrega la clase form-control de bootstrap a los input"""
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
    

