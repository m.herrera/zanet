from django.contrib import admin
from .models import Modem, Antena, Emisora
# Register your models here.


class ModemAdmin(admin.ModelAdmin):
    """ modelo administrativo de modem"""
    ordering = ['id']
    list_display = ('id', 'marca', 'clave_wifi', 'gateway', 'cliente', 'imagen')
    list_filter = ['marca']
    list_per_page = 10


class AntenaAdmin(admin.ModelAdmin):
    """ modelo administrativo de antena"""
    ordering = ['id']
    list_display = ('id', 'marca', 'cliente', 'emisora', 'ip_interna',
                    'ip_externa', 'gateway', 'imagen', 'signal_min', 'signal_actual')
    list_filter = ['marca']
    list_per_page = 10


class EmisoraAdmin(admin.ModelAdmin):
    """ modelo administrativo de emisora"""
    ordering = ['id']
    list_display = ('id', 'nombre', 'direccion_ip',
                    'ancho_de_canal', 'frecuencia')
    list_filter = ['ancho_de_canal', 'frecuencia']
    list_per_page = 10


admin.site.register(Modem, ModemAdmin)
admin.site.register(Antena, AntenaAdmin)
admin.site.register(Emisora, EmisoraAdmin)
