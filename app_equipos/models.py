from django.db import models
from app_clientes.models import Cliente

# Create your models here.
class Modem(models.Model):
    """ modelo que representa un modem"""
    marca = models.CharField(max_length=45, null=False, blank=False)
    clave_wifi = models.CharField(max_length=45, null=True, blank=True)
    gateway = models.CharField(max_length=45, null=True, blank=True)
    imagen = models.ImageField(upload_to='modems', null=True, blank=True)
    notas = models.CharField(max_length=200, null=True, blank=True)

    # relaciones
    # un cliente recibe 1 o mas modems en su instalación
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)    

    def __str__(self):
        """ Descripción del modelo modem """
        return '{}'.format(self.marca)

    # def save(self):
    #     """ el nombre del modem lo guardamos en mayúsculas"""
    #     self.marca = self.marca.upper()
    #     super(Modem, self).save()

    class Meta:
        """ nombre en plural(Muchos) del modelo"""
        verbose_name_plural = "Modems"


class Emisora(models.Model):
    """ modelo que representa una emisora"""
    nombre = models.CharField(max_length=45, blank=False, null=False)
    ssid = models.CharField(max_length=45, blank=True, null=True)
    direccion_mac = models.CharField(max_length=45, blank=True, null=True)
    direccion_ip = models.CharField(max_length=45, blank=True, null=True)
    ancho_de_canal = models.CharField(max_length=45, blank=True, null=True)
    frecuencia = models.IntegerField(blank=True, null=True)
    ubicacion = models.CharField(max_length=45, blank=True, null=True)
    notas = models.CharField(max_length=200, null=True, blank=True)    

    def __str__(self):
        """ Descripción del modelo emisora """
        return '{}'.format(self.nombre)

    class Meta:
        """ nombre en plural(Muchos) del modelo"""
        verbose_name_plural = "Emisoras"


class Antena(models.Model):
    """ modelo que representa una antena"""
    marca = models.CharField(max_length=45, null=False, blank=False)
    ip_interna = models.CharField(max_length=45, null=True, blank=True)
    ip_externa = models.CharField(max_length=45, null=True, blank=True)
    gateway = models.CharField(max_length=45, null=True, blank=True)
    signal_min = models.IntegerField(null=True, blank=True)
    signal_actual = models.IntegerField(null=True, blank=True)
    imagen = models.ImageField(upload_to='antenas', null=True, blank=True)
    notas = models.CharField(max_length=200, null=True, blank=True)

    # relaciones
    # un cliente recibe 1 antena en su instalación
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    # una antena esta conectada en una emisora
    emisora = models.ForeignKey(Emisora, on_delete=models.CASCADE)

    def __str__(self):
        """ Descripción del modelo antena """
        return '{}'.format(self.marca)

    # def save(self):
    #     """ el nombre de la antena lo guardamos en mayúsculas"""
    #     self.marca = self.marca.upper()
    #     super(Antena, self).save()

    class Meta:
        """ nombre en plural(Muchos) del modelo"""
        verbose_name_plural = "Antenas"

