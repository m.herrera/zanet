from django.contrib.admin.widgets import AutocompleteSelect
from django.contrib import admin
from django import forms
from django.core.exceptions import ValidationError
from .models import *

class ModemsForm(forms.ModelForm):
    """ form de modems"""
    
    class Meta:
        model = Modem
        fields = '__all__'  
        
    def __init__(self, *args, **kwargs):
        """ método que itera los campos del form y agrega la clase form-control de bootstrap a los input"""
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            }) 
        
        # al select y/o combo box de forma_de_pago le agregamos un valor por defecto        
        self.fields['cliente'].empty_label = 'Seleccione el cliente..' 
        
class AntenasForm(forms.ModelForm):
    """ form de antenas"""
    
    class Meta:
        model = Antena
        fields = '__all__' 
        
    def __init__(self, *args, **kwargs):
        """ método que itera los campos del form y agrega la clase form-control de bootstrap a los input"""
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            }) 
        
        # al select y/o combo box de cliente le agregamos un valor por defecto        
        self.fields['cliente'].empty_label = 'Seleccione el cliente..' 
        self.fields['emisora'].empty_label = 'Seleccione la emisora..' 
        
class EmisorasForm(forms.ModelForm):
    """ form de emisoras"""
    
    class Meta:
        model = Emisora
        fields = '__all__' 
        
    def __init__(self, *args, **kwargs):
        """ método que itera los campos del form y agrega la clase form-control de bootstrap a los input"""
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            }) 
        
        
