
from django.urls import path
from .views import *

urlpatterns = [
    # crud de modems
    path('modems/', ModemView.as_view(), name='modems_list'),
    path('modems/new', ModemNew.as_view(), name='modems_new'),
    path('modems/edit/<int:pk>', ModemEdit.as_view(), name='modems_edit'),
    path('modems/delete/<int:pk>', ModemDelete.as_view(), name='modems_delete'),
    
    # crud de antenas
    path('antenas/', AntenaView.as_view(), name='antenas_list'),
    path('antenas/new', AntenaNew.as_view(), name='antenas_new'),
    path('antenas/edit/<int:pk>', AntenaEdit.as_view(), name='antenas_edit'),
    path('antenas/delete/<int:pk>', AntenaDelete.as_view(), name='antenas_delete'),
    
    # crud de emisoras
    path('emisoras/', EmisoraView.as_view(), name='emisoras_list'),
    path('emisoras/new', EmisoraNew.as_view(), name='emisoras_new'),
    path('emisoras/edit/<int:pk>', EmisoraEdit.as_view(), name='emisoras_edit'),
    path('emisoras/delete/<int:pk>', EmisoraDelete.as_view(), name='emisoras_delete'),
]