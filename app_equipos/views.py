from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import *
from django.urls import reverse_lazy
from django.contrib import messages
from .models import *
from .views import *
from .forms import *
from bases.views import Sin_Privilegios

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin

class ModemView(Sin_Privilegios, ListView):
    """ vista basada en clase para listar modems"""
    permission_required = "app_equipos.view_modem"
    model = Modem
    template_name = "app_equipos/modems_list.html"
    context_object_name = "obj" 
    
class ModemNew(Sin_Privilegios, SuccessMessageMixin, CreateView):
    """ vista basada en clase para llenar form de modems"""
    permission_required = "app_equipos.add_modem"
    model = Modem
    template_name = "app_equipos/modems_form.html"
    context_object_name = "obj"
    form_class = ModemsForm 
    success_message = "Modem agregado satisfactoriamente.."
    success_url = reverse_lazy("app_equipos:modems_list") 
    
class ModemEdit(Sin_Privilegios, SuccessMessageMixin, UpdateView):
    """ vista basada en clase para editar form de modems"""
    permission_required = "app_equipos.change_modem"
    model = Modem
    template_name = "app_equipos/modems_form.html"
    context_object_name = "obj"
    form_class = ModemsForm   
    success_message = "Modem actualizado satisfactoriamente.."
    success_url = reverse_lazy("app_equipos:modems_list")
    
class ModemDelete(Sin_Privilegios, SuccessMessageMixin, DeleteView):
    """ vista basada en clase para eliminar un modem"""
    permission_required = "app_equipos.delete_modem"
    model = Modem
    template_name = "app_equipos/modems_del.html"
    context_object_name = "obj"
    success_message = "Modem eliminado satisfactoriamente.."
    success_url = reverse_lazy("app_equipos:modems_list")
    
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
    
class AntenaView(Sin_Privilegios, ListView):
    """ vista basada en clase para listar las antenas"""
    permission_required = "app_equipos.view_antena"
    model = Antena
    template_name = "app_equipos/antenas_list.html"
    context_object_name = "obj" 
    
class AntenaNew(Sin_Privilegios, SuccessMessageMixin, CreateView):
    """ vista basada en clase para llenar form de antenas"""
    permission_required = "app_equipos.add_antena"
    model = Antena
    template_name = "app_equipos/antenas_form.html"
    context_object_name = "obj"
    form_class = AntenasForm
    success_message = "Antena agregada satisfactoriamente.."
    success_url = reverse_lazy("app_equipos:antenas_list") 
    
class AntenaEdit(Sin_Privilegios, SuccessMessageMixin, UpdateView):
    """ vista basada en clase para editar form de antenas"""
    permission_required = "app_equipos.change_antena"
    model = Antena
    template_name = "app_equipos/antenas_form.html"
    context_object_name = "obj"
    form_class = AntenasForm  
    success_message = "Antena actualizada satisfactoriamente.."
    success_url = reverse_lazy("app_equipos:antenas_list")
    
class AntenaDelete(Sin_Privilegios, SuccessMessageMixin, DeleteView):
    """ vista basada en clase para eliminar una antena"""
    permission_required = "app_equipos.delete_antena"
    model = Antena
    template_name = "app_equipos/antenas_del.html"
    context_object_name = "obj"
    success_message = "Antena eliminada satisfactoriamente.."
    success_url = reverse_lazy("app_equipos:antenas_list")
    
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
    
class EmisoraView(Sin_Privilegios, ListView):
    """ vista basada en clase para listar las emisoras"""
    permission_required = "app_equipos.view_emisora"
    model = Emisora
    template_name = "app_equipos/emisoras_list.html"
    context_object_name = "obj" 
    
class EmisoraNew(Sin_Privilegios, SuccessMessageMixin, CreateView):
    """ vista basada en clase para llenar form de emisoras"""
    permission_required = "app_equipos.add_emisora"
    model = Emisora
    template_name = "app_equipos/emisoras_form.html"
    context_object_name = "obj"
    form_class = EmisorasForm
    success_message = "Emisora agregada satisfactoriamente.."
    success_url = reverse_lazy("app_equipos:emisoras_list") 
    
class EmisoraEdit(Sin_Privilegios, SuccessMessageMixin, UpdateView):
    """ vista basada en clase para editar form de emisoras"""
    permission_required = "app_equipos.change_emisora"
    model = Emisora
    template_name = "app_equipos/emisoras_form.html"
    context_object_name = "obj"
    form_class = EmisorasForm  
    success_message = "Emisora actualizada satisfactoriamente.."
    success_url = reverse_lazy("app_equipos:emisoras_list")
    
class EmisoraDelete(Sin_Privilegios, SuccessMessageMixin, DeleteView):
    """ vista basada en clase para eliminar una emisora"""
    permission_required = "app_equipos.delete_emisora"
    model = Emisora
    template_name = "app_equipos/emisoras_del.html"
    context_object_name = "obj"
    success_message = "Emisora eliminada satisfactoriamente.."
    success_url = reverse_lazy("app_equipos:emisoras_list")