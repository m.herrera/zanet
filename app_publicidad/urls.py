from django.urls import path
from django.contrib.auth import views as auth_views
from app_publicidad.views import *

app_name = 'publicidad'

urlpatterns = [
    path('planes/', get_planes, name="planes_list"),    
    path('contactanos/', contactanos, name="contactos_list"),    
]