from django.shortcuts import render
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

# Create your views here.
@login_required(login_url='/login/') # debe estar logeado
def get_planes(request):
    """ vbf para planes"""
    template_name = "app_publicidad/planes.html"
    context = {}
    return render(request, template_name, context)

@login_required(login_url='/login/') # debe estar logeado
def contactanos(request):
    """ vbf para coontactos y ubicaciones de oficina"""
    template_name = "app_publicidad/contactanos.html"
    context = {}
    return render(request, template_name, context)