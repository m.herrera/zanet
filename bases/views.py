
from django.shortcuts import render, redirect
from django.views.generic import *
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from app_clientes.models import *
from app_equipos.models import *
from logging import raiseExceptions
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy

# Create your views here.
class Home(LoginRequiredMixin, TemplateView):
    """ vista que renderiza a la plantilla base"""
    template_name = "bases/home.html"
    login_url = 'bases:login'
    
class Sin_Privilegios(LoginRequiredMixin, PermissionRequiredMixin):
    login_url = 'bases:login'
    raiseExceptions = False # no se ponga la pantalla en blanco
    redirect_field_name = "redirect_to"

    def handle_no_permission(self):
        """ método que valida si el usuario tiene privilegios, para redireccionar a plantilla"""
        from django.contrib.auth.models import AnonymousUser

        # si el usuario es válido, se muestra la vista sin privilegios
        if not self.request.user == AnonymousUser():
            self.login_url = "config:sin_privilegios"

        return HttpResponseRedirect(reverse_lazy(self.login_url))
    

class HomeSinPrivilegios(LoginRequiredMixin, TemplateView):
    login_url = 'bases:login'
    template_name = "bases/sin_privilegios.html"
    
def get_estadisticas(request):
    """" vista que obtiene las estadísticas generales"""
    
    # app_clientes
    total_de_clientes = Cliente.objects.all().count()
    total_de_pagos = Pago.objects.all().count()
    total_de_instalaciones = Instalacion.objects.all().count()
    total_de_localidades = Localidad.objects.all().count()
    total_de_cajeros = Contador.objects.all().count()
    
    # app_equipos
    total_de_modems = Modem.objects.all().count()
    total_de_antenas = Antena.objects.all().count()
    total_de_emisoras = Emisora.objects.all().count()
        
    template_name = "bases/home.html"
    context = {
        'clientes': total_de_clientes,
        'pagos': total_de_pagos,
        'instalaciones': total_de_instalaciones,
        'localidades': total_de_localidades,
        'cajeros': total_de_cajeros,
        'modems': total_de_modems,
        'antenas': total_de_antenas,
        'emisoras': total_de_emisoras
    }
    
    return render(request, template_name, context)

class ClientesView(LoginRequiredMixin, TemplateView):
    """ vista basada en clase para listar clientes"""   
    template_name = "bases/img-clientes.html"
    
    
    
    
