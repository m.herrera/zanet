
from django.urls import path
from django.contrib.auth import views as auth_views
from bases.views import *

app_name = "config"

urlpatterns = [
    path('', Home.as_view(), name='home'),
    path('bases/obtener-estadisticas/', get_estadisticas, name="home_estadisticas"),
    path('login/', auth_views.LoginView.as_view(template_name = 'bases/login.html'), name="login"),
    path('logout/', auth_views.LogoutView.as_view(template_name = 'bases/login.html'), name="logout"),
    path('sin_privilegios/', HomeSinPrivilegios.as_view(), name="sin_privilegios"),
    path('listado-de-clientes/', ClientesView.as_view(), name="listado-de-clientes"),
]