# Páginas utiles
- https://www.django-rest-framework.org/tutorial/3-class-based-views/
- https://www.cdrf.co/

# Url de video
https://www.youtube.com/watch?v=Xts8NmyAc8c

# Notas importantes
## instalar los siguentes paquetes
* pip install djangorestframework
* pip install markdown       # Markdown support for the browsable API.
* pip install django-filter  # Filtering support
* pip install coreapi

## agregar la app
```
INSTALLED_APPS = [
    'rest_framework',
]
```

## configuracion en settings.py, solo para usuarios autenticados
```
REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}
```

# Lógica de la API
* crear el serializer
```
from rest_framework import serializers
from app_clientes.models import Localidad, Cliente

class LocalidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Localidad
        fields = '__all__'
        
class ClienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cliente
        fields = '__all__'
```

* crear las vistas
```
from django.shortcuts import render
from rest_framework import viewsets
from api.serializer import LocalidadSerializer, ClienteSerializer
from app_clientes.models import Localidad, Cliente

# Create your views here.
class LocalidadsViewSet(viewsets.ModelViewSet):
    queryset = Localidad.objects.all()
    serializer_class = LocalidadSerializer
    
class ClienteViewSet(viewsets.ModelViewSet):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer
```

* crear las rutas
```
from django.urls import path, include
from rest_framework import routers
from api import views

# Routers - provee una manera facil de redirigir a los endpoint
# metodos accecibles GET, POST, PUT, DELETE
router = routers.DefaultRouter()
router.register(r'localidades', views.LocalidadsViewSet)
router.register(r'clientes', views.ClienteViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
```

* y por ultimo, incluir las rutas en el archivo global urls.py
```
urlpatterns = [
    path('api/v1/', include('api.urls')),
]
```

# Documentación de API
## instalamos coreapi
- pip install coreapi

## agregamos la app
```
INSTALLED_APPS = [
    'coreapi',
]
```

## configuramos la url
```
from rest_framework.documentation import include_docs_urls
urlpatterns = [
    path('docs/', include_docs_urls(title='API Documentación')),
]
```

## error
- 'AutoSchema' object has no attribute 'get_link'
## solución: agregar esto en settings.py
```
REST_FRAMEWORK = {
  'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema'
}
```

- PUT: actualiza todo el elemento
- PATCH: actualiza solo una parte del elemento