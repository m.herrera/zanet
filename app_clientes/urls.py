
from django.urls import path
from .views import *

urlpatterns = [
    # crud de localidades
    path('localidades/', LocalidadView.as_view(), name='localidades_list'),
    path('localidades/new', LocalidadNew.as_view(), name='localidades_new'),
    path('localidades/edit/<int:pk>', LocalidadEdit.as_view(), name='localidades_edit'),
    path('localidades/delete/<int:pk>', LocalidadDelete.as_view(), name='localidades_delete'),
    
    # crud de clientes
    path('clientes/', ClienteView.as_view(), name='clientes_list'),
    path('clientes/new', ClienteNew.as_view(), name='clientes_new'),
    path('clientes/edit/<int:pk>', ClienteEdit.as_view(), name='clientes_edit'),
    path('clientes/delete/<int:pk>', ClienteDelete.as_view(), name='clientes_delete'),
    
    # agregar pago de un cliente
    path('clientes/agregar-pago/<int:id>', agregar_pago, name='agregar_pago'),
    
    # modems configurados de cada cliente
    path('clientes/modems_por_cliente/<int:id>', modems_por_cliente, name='modems_por_cliente'),       
    
    # antenas instaladas a un cliente
    path('clientes/antenas_por_cliente/<int:id>', antenas_por_cliente, name='antenas_por_cliente'),
    
    # historial de pagos de un cliente
    path('clientes/historial_de_pagos/<int:id>', pagos_por_cliente, name='pagos_por_cliente'),
    
    # detalles de instalación de un cliente
    path('clientes/detalles-instalacion/<int:id>', detalles_de_instalacion, name='detalles_de_instalacion'),
    
    # crud de cajeros
    path('cajeros/', ContadorView.as_view(), name='cajeros_list'),
    path('cajeros/new', ContadordNew.as_view(), name='cajeros_new'),
    path('cajeros/edit/<int:pk>', ContadorEdit.as_view(), name='cajeros_edit'),
    path('cajeros/delete/<int:pk>', ContadorDelete.as_view(), name='cajeros_delete'),    
    
    # crud de instalaciones
    path('instalaciones/', InstalacionView.as_view(), name='instalaciones_list'),
    path('instalaciones/new', InstalacionNew.as_view(), name='instalaciones_new'),
    path('instalaciones/edit/<int:pk>', InstalacionEdit.as_view(), name='instalaciones_edit'),
    path('instalaciones/delete/<int:pk>', InstalacionDelete.as_view(), name='instalaciones_delete'),
    
    # crud de pagos
    path('pagos/', PagosView.as_view(), name='pagos_list'),
    path('pagos/new', PagosnNew.as_view(), name='pagos_new'),
    path('pagos/edit/<int:pk>', PagosEdit.as_view(), name='pagos_edit'),
    path('pagos/delete/<int:pk>', PagosDelete.as_view(), name='pagos_delete'),
]