# Generated by Django 4.2.14 on 2024-12-10 04:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_clientes', '0012_instalacion_accion'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='localidad',
            unique_together={('nombre',)},
        ),
    ]
