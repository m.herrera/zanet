from django.contrib.admin.widgets import AutocompleteSelect
from django.contrib import admin
from django import forms
from django.core.exceptions import ValidationError
from .models import *

class LocalidadesForm(forms.ModelForm):
    """ form para Localidades"""
    # validaciones en el servidor
    nombre = forms.CharField(required=True)
    
    class Meta:
        model = Localidad        
        # campos que muestra el formulario
        fields = ("nombre",)
        labels = {"nombre": "Nombre"}        
        widget = {'nombre': forms.TextInput}

    def __init__(self, *args, **kwargs):
        """ método que itera los campos del form y agrega la clase form-control de bootstrap a los input"""
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
                                
class ClientesForm(forms.ModelForm): 
    """ form para clientes"""    
    
    class Meta:
        model = Cliente
        fields = '__all__'  
        
    def __init__(self, *args, **kwargs):
        """ método que itera los campos del form y agrega la clase form-control de bootstrap a los input"""
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            }) 
        
        # al select y/o combo box de localidades le agregamos un valor por defecto
        self.fields['localidad'].empty_label = 'Seleccione la localidad..'  
                
                
class ContadoresForm(forms.ModelForm): 
    """ form para cajeros"""    
    
    class Meta:
        model = Contador
        fields = '__all__'  
        
    def __init__(self, *args, **kwargs):
        """ método que itera los campos del form y agrega la clase form-control de bootstrap a los input"""
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            }) 
        
        # al select y/o combo box de localidades le agregamos un valor por defecto
        self.fields['localidad'].empty_label = 'Seleccione localidad..'  
        
class InstalacionesForm(forms.ModelForm):
    """ form de instalaciones"""
    
    class Meta:
        model = Instalacion
        fields = '__all__'  
        
    def __init__(self, *args, **kwargs):
        """ método que itera los campos del form y agrega la clase form-control de bootstrap a los input"""
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            }) 
        
        # al select y/o combo box de forma_de_pago le agregamos un valor por defecto        
        self.fields['cliente'].empty_label = 'Seleccione el cliente.' 
        
class PagosForm(forms.ModelForm):
    """ form de pagos"""
    
    class Meta:
        model = Pago
        fields = '__all__' 
        
    def __init__(self, *args, **kwargs):
        """ método que itera los campos del form y agrega la clase form-control de bootstrap a los input"""
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })         
        
        # al select y/o combo box de forma_de_pago le agregamos un valor por defecto        
        self.fields['cliente'].empty_label = 'Seleccione el cliente..'  
        self.fields['contador'].empty_label = 'Seleccione el contador..'
         
        

               
        

        