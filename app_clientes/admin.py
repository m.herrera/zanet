from django.contrib import admin
from .models import Localidad, Instalacion, Cliente, Contador, Pago

# Register your models here.
class LocalidadAdmin(admin.ModelAdmin):
    """ modelo administrativo de Localidad"""
    ordering = ['id']
    list_display = ('id', 'nombre')
    search_fields = ['nombre']
    list_per_page = 10
    
class InstalaciónAdmin(admin.ModelAdmin):
    """ modelo administrativo de Instalación"""
    ordering = ['id']
    list_display = ('id', 'cliente', 'fecha', 'forma_de_pago', 'monto_recibido', 'notas')
    list_filter = ['cliente', 'fecha', 'forma_de_pago']
    search_fields = ['id', 'fecha']
    list_per_page = 10
    
class ClienteAdmin(admin.ModelAdmin):
    """ modelo administrativo del cliente"""
    ordering = ['id']
    list_display = ('id', 'nombres', 'ap_paterno', 'ap_materno', 'fecha_de_pago', 'localidad')
    list_filter = ['localidad', 'fecha_de_pago']
    search_fields = ['nombres']
    list_per_page = 10
    
class ContadorAdmin(admin.ModelAdmin):
    """ modelo administrativo del contador"""
    ordering = ['id']
    list_display = ('id', 'nombres', 'ap_paterno', 'ap_materno', 'telefono', 'correo', 'localidad')
    search_fields = ['nombres']
    list_per_page = 10
    
class PagoAdmin(admin.ModelAdmin):
    """ modelo administrativo de pagos"""
    ordering = ['id']
    list_display = ('id', 'cliente', 'contador', 'fecha_de_emision', 'concepto', 'total')
    list_filter = ['cliente']  
    list_per_page = 10
         
admin.site.register(Localidad, LocalidadAdmin)
admin.site.register(Instalacion, InstalaciónAdmin)
admin.site.register(Cliente, ClienteAdmin)
admin.site.register(Contador, ContadorAdmin)
admin.site.register(Pago, PagoAdmin)
