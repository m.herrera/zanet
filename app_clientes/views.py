from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import *
from django.urls import reverse_lazy
from django.contrib import messages
from .models import *
from app_equipos.models import *
from .views import *
from .forms import *
from bases.views import Sin_Privilegios

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin

# Create your views here.
class LocalidadView(Sin_Privilegios, ListView):
    """ vista basada en clase para listar clientes"""
    permission_required = "app_clientes.view_localidad"
    model = Localidad
    template_name = "app_clientes/localidades_list.html"
    context_object_name = "obj"     
    
class LocalidadNew(Sin_Privilegios, SuccessMessageMixin, CreateView):
    """ vista basada en clase para llenar form de localidad"""
    permission_required = "app_clientes.add_localidad"
    model = Localidad
    template_name = "app_clientes/localidades_form.html"
    context_object_name = "obj"
    form_class = LocalidadesForm        
    success_message = "Localidad creada satisfactoriamente.."
    success_url = reverse_lazy("app_clientes:localidades_list") 
    
class LocalidadEdit(Sin_Privilegios, SuccessMessageMixin, UpdateView):
    """ vista basada en clase para editar form de localidad"""
    permission_required = "app_clientes.change_localidad"
    model = Localidad
    template_name = "app_clientes/localidades_form.html"
    context_object_name = "obj"
    form_class = LocalidadesForm    
    success_message = "Localidad actualizada satisfactoriamente.."    
    success_url = reverse_lazy("app_clientes:localidades_list")
    
class LocalidadDelete( Sin_Privilegios, SuccessMessageMixin, DeleteView):
    """ vista basada en clase para eliminar una localidad"""
    permission_required = "app_clientes.delete_localidad"
    model = Localidad
    template_name = "app_clientes/localidades_del.html"
    context_object_name = "obj"
    success_message = "Localidad eliminada satisfactoriamente.."
    success_url = reverse_lazy("app_clientes:localidades_list")
    
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------

class ClienteView(Sin_Privilegios, ListView):
    """ vista basada en clase para listar clientes"""
    permission_required = "app_clientes.view_cliente"
    model = Cliente
    template_name = "app_clientes/clientes_list.html"
    context_object_name = "obj" 
    
class ClienteNew(Sin_Privilegios, SuccessMessageMixin, CreateView):
    """ vista basada en clase para llenar form de clientes"""
    permission_required = "app_clientes.add_cliente"
    model = Cliente
    template_name = "app_clientes/clientes_form.html"
    context_object_name = "obj"
    form_class = ClientesForm    
    success_message = "Cliente agregado satisfactoriamente.."
    success_url = reverse_lazy("app_clientes:clientes_list") 
    
class ClienteEdit(Sin_Privilegios, SuccessMessageMixin, UpdateView):
    """ vista basada en clase para editar form de clientes"""
    permission_required = "app_clientes.change_cliente"
    model = Cliente
    template_name = "app_clientes/clientes_form.html"
    context_object_name = "obj"
    form_class = ClientesForm
    success_message = "Cliente actualizado satisfactoriamente.."
    success_url = reverse_lazy("app_clientes:clientes_list")
    
   
class ClienteDelete(Sin_Privilegios, SuccessMessageMixin, DeleteView):
    """ vista basada en clase para eliminar un cliente"""
    permission_required = "app_clientes.delete_cliente"
    model = Cliente
    template_name = "app_clientes/clientes_del.html"
    context_object_name = "obj"
    success_message = "Cliente eliminado satisfactoriamente.."
    success_url = reverse_lazy("app_clientes:clientes_list")
    
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
        
class ContadorView(Sin_Privilegios, ListView):
    """ vista basada en clase para listar cajeros"""
    permission_required = "app_clientes.view_contador"
    model = Contador
    template_name = "app_clientes/contadores_list.html"
    context_object_name = "obj" 
    
class ContadordNew(Sin_Privilegios, SuccessMessageMixin, CreateView):
    """ vista basada en clase para llenar form de cajeros"""
    permission_required = "app_clientes.add_contador"
    model = Contador
    template_name = "app_clientes/cajeros_form.html"
    context_object_name = "obj"
    form_class = ContadoresForm    
    success_message = "Cajero agregado satisfactoriamente.."
    success_url = reverse_lazy("app_clientes:cajeros_list") 
    
class ContadorEdit(Sin_Privilegios, SuccessMessageMixin, UpdateView):
    """ vista basada en clase para editar form de cajeros"""
    permission_required = "app_clientes.change_contador"
    model = Contador
    template_name = "app_clientes/cajeros_form.html"
    context_object_name = "obj"
    form_class = ContadoresForm
    success_message = "Cajero actualizado satisfactoriamente.."
    success_url = reverse_lazy("app_clientes:cajeros_list")
    
class ContadorDelete(Sin_Privilegios, SuccessMessageMixin, DeleteView):
    """ vista basada en clase para eliminar un cajero"""
    permission_required = "app_clientes.delete_contador"
    model = Contador
    template_name = "app_clientes/cajeros_del.html"
    context_object_name = "obj"
    success_message = "Cajero eliminado satisfactoriamente.."
    success_url = reverse_lazy("app_clientes:cajeros_list")
    
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
    
class InstalacionView(Sin_Privilegios, ListView):
    """ vista basada en clase para listar las instalaciones realizadas"""
    permission_required = "app_clientes.view_instalacion"
    model = Instalacion
    template_name = "app_clientes/instalaciones_list.html"
    context_object_name = "obj" 
    
class InstalacionNew(Sin_Privilegios, SuccessMessageMixin, CreateView):
    """ vista basada en clase para llenar form de instalaciones"""
    permission_required = "app_clientes.add_instalacion"
    model = Instalacion
    template_name = "app_clientes/instalaciones_form.html"
    context_object_name = "obj"
    form_class = InstalacionesForm  
    success_message = "Instalación agregada satisfactoriamente.."
    success_url = reverse_lazy("app_clientes:instalaciones_list") 
    
class InstalacionEdit(Sin_Privilegios, SuccessMessageMixin, UpdateView):
    """ vista basada en clase para editar form de instalacón"""
    permission_required = "app_clientes.change_instalacion"
    model = Instalacion
    template_name = "app_clientes/instalaciones_form.html"
    context_object_name = "obj"
    form_class = InstalacionesForm
    success_message = "Instalación actualizada satisfactoriamente.."
    success_url = reverse_lazy("app_clientes:instalaciones_list")

class InstalacionDelete(Sin_Privilegios, SuccessMessageMixin, DeleteView):
    """ vista basada en clase para eliminar la instalación de un cliente"""
    permission_required = "app_clientes.delete_instalacion"
    model = Instalacion
    template_name = "app_clientes/instalacion_del.html"
    context_object_name = "obj"
    success_message = "Instalación eliminada satisfactoriamente.."
    success_url = reverse_lazy("app_clientes:instalaciones_list")
    
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------
    
class PagosView(Sin_Privilegios, ListView):
    """ vista basada en clase para listar los pagos realizados"""
    permission_required = "app_clientes.view_pago"
    model = Pago
    template_name = "app_clientes/pagos_list.html"
    context_object_name = "obj" 
    
class PagosnNew(Sin_Privilegios, SuccessMessageMixin, CreateView):
    """ vista basada en clase para llenar form de pagos"""
    permission_required = "app_clientes.add_pago"
    model = Pago
    template_name = "app_clientes/pagos_form.html"
    context_object_name = "obj"
    form_class = PagosForm 
    success_message = "Pago agregado satisfactoriamente.."
    success_url = reverse_lazy("app_clientes:pagos_list") 
    
class PagosEdit(Sin_Privilegios, SuccessMessageMixin, UpdateView):
    """ vista basada en clase para editar form de pagos"""
    permission_required = "app_clientes.change_pago"
    model = Pago
    template_name = "app_clientes/pagos_form.html"
    context_object_name = "obj"
    form_class = PagosForm
    success_message = "Pago actualizado satisfactoriamente.."
    success_url = reverse_lazy("app_clientes:pagos_list")
    
class PagosDelete(Sin_Privilegios, SuccessMessageMixin, DeleteView):
    """ vista basada en clase para eliminar el pago de un cliente"""
    permission_required = "app_clientes.delete_pago"
    model = Pago
    template_name = "app_clientes/pagos_del.html"
    context_object_name = "obj"
    success_message = "Pago eliminado satisfactoriamente.."
    success_url = reverse_lazy("app_clientes:pagos_list")
    
# ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------

# INNER JOIN
# consultamos todos los modem asignados a un cliente
@login_required(login_url='/login/') # debe estar logeado
@permission_required('app_equipos.view_modem', login_url='bases:sin_privilegios')
def modems_por_cliente(request, id):
    """ mostramos todos los modems que tiene configurados un cliente"""    
    modems = Modem.objects.all().select_related('cliente').filter(cliente=id)
    cliente = Cliente.objects.all().filter(id=id).first();
    #print(modems)
    #print(cliente)
    
    context = {
        'obj': modems,
        'cliente': cliente
    }
    template_name = "app_clientes/modems_de_cliente.html"
    return render(request, template_name, context)

# consultar el tipo de antenas instaladas a un cliente especifico
@login_required(login_url='/login/') # debe estar logeado
@permission_required('app_equipos.view_antena', login_url='bases:sin_privilegios')
def antenas_por_cliente(request, id):
    """ mostramos la antena que tiene un cliente"""
    antenas = Antena.objects.all().select_related('cliente').filter(cliente=id)
    cliente = Cliente.objects.all().filter(id=id).first();
    # print(antenas)
    print(cliente)
    
    context = {
        'obj': antenas,
        'cliente': cliente    
    }
    template_name = "app_clientes/antena_de_cliente.html"
    return render(request, template_name, context)

# consultar todos los pagos realizados por un cliente
@login_required(login_url='/login/') # debe estar logeado
@permission_required('app_clientes.view_pago', login_url='bases:sin_privilegios')
def pagos_por_cliente(request, id):
    """ mostramos el historial de pagos de un cliente especifico"""
    pagos = Pago.objects.all().select_related('cliente').filter(cliente=id)
    print(pagos)
    
    context = {'obj': pagos}
    template_name = "app_clientes/pagos_por_cliente.html"
    return render(request, template_name, context)

# consultar los detalles de instalación de un cliente
@login_required(login_url='/login/')
@permission_required('app_clientes.view_instalacion', login_url='bases:sin_privilegios')
def detalles_de_instalacion(request, id):
    """ mostramos los detalles de instalación de un cliente especifico"""
    instalacion = Instalacion.objects.all().select_related('cliente').filter(cliente=id)
    print(instalacion)
    
    context = {'obj': instalacion}
    template_name = "app_clientes/detalles_de_instalacion.html"
    return render(request, template_name, context)
    
@login_required(login_url='/login/')
@permission_required('app_clientes.add_pago', login_url='bases:sin_privilegios')
def agregar_pago(request, id):
    """ form para agregar pago de un cliente determinado"""
    
    # cliente que va a pagar
    cliente = Cliente.objects.all().filter(id=id)
    print(cliente)
    # cajero que va a recibir el pago
    cajeros = Contador.objects.all()
    print(cajeros)
    
    data = {
        'form': PagosForm(),
        'cliente': cliente,
        'cajeros': cajeros
    }
    
    if request.method == "POST":
        formulario = PagosForm(data=request.POST or None, files=request.FILES)
        if formulario.is_valid():            
            formulario.save()
            messages.success(request, 'Pago agregado correctamente..')
            return redirect('app_clientes:clientes_list')
        else:
            messages.error(request, "Error en el envio de datos!!")
            data['form'] = formulario
    
    return render(request, 'app_clientes/agregar_pago.html', data)
    
    