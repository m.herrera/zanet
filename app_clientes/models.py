from django.db import models

# Create your models here.
class Localidad(models.Model):
    """ modelo localidad"""
    nombre = models.CharField(max_length=45)

    def __str__(self):
        """ Descripción del modelo localidad """
        return '{}'.format(self.nombre)        

    class Meta:
        """ nombre en plural(Muchos) del modelo"""
        verbose_name_plural = "Localidades"        


class Cliente(models.Model):
    """ modelo que representa un cliente"""
    nombres = models.CharField(max_length=45, null=False, blank=False)
    ap_paterno = models.CharField(max_length=45, null=False, blank=False)
    ap_materno = models.CharField(max_length=45, null=True, blank=True)
    fecha_de_pago = models.DateField(null=False, blank=False)

    ACT = 'ACTIVO'
    INA = 'INACTIVO'
    ESTATUS = [
        (ACT, 'ACTIVO'),
        (INA, 'INACTIVO')
    ]

    estatus = models.CharField(
        max_length=12,
        choices=ESTATUS,
        default=ACT
    )

    M = 'MASCULINO'
    F = 'FEMENINO'
    O = 'OTROS'
    GENERO = [
        (M, 'MASCULINO'),
        (F, 'FEMENINO'),
        (O, 'OTROS')
    ]
    genero = models.CharField(
        max_length=12,
        choices=GENERO,
        default=M
    )

    domicilio = models.CharField(max_length=100, null=True, blank=True)
    telefono = models.BigIntegerField(
        null=True,
        blank=True
    )
    correo = models.EmailField(null=True, blank=True)
    monto_a_pagar = models.IntegerField(null=True, blank=True)
    notas = models.CharField(max_length=100, null=True, blank=True)

    # relaciones
    # un cliente pertenece a una localidad
    localidad = models.ForeignKey(
        Localidad, on_delete=models.CASCADE, default="")
    # un cliente solicita 1 o varias instalaciones
    # instalacion = models.ForeignKey(Instalacion, on_delete=models.CASCADE)

    def __str__(self): 
        """ Descripción del modelo cliente """
        return '{} {} {}'.format(self.nombres, self.ap_paterno, self.ap_materno)

    # def save(self):
    #     """ el nombre del cliente lo guardamos en mayúsculas"""
    #     self.nombres = self.nombres.upper()
    #     if self.ap_paterno:  # si la cadena esta llena
    #         self.ap_paterno = self.ap_paterno.upper()

    #     if self.ap_materno:  # si la cadena esta llena
    #         self.ap_materno = self.ap_materno.upper()

    #     super(Cliente, self).save()

    class Meta:
        """ nombre en plural(Muchos) del modelo"""
        verbose_name_plural = "Clientes"


class Instalacion(models.Model):
    """ modelo que representa una instalación"""
    fecha = models.DateField(null=False, blank=False)
    
    # choice action
    INSTALACION = "INSTALACION"
    RECONEXION = "RECONEXION"
    ACCION = [
        (INSTALACION, "INSTALACION"),
        (RECONEXION, "RECONEXION")
    ]
    
    accion = models.CharField(
        max_length=20,
        choices=ACCION,
        default=INSTALACION
    )
    
    # choices
    PENDIENTE = 'PENDIENTE'
    FINALIZADA = 'FINALIZADA'
    ESTATUS = [
        (PENDIENTE, 'PENDIENTE'),
        (FINALIZADA, 'FINALIZADA')
    ]

    estatus = models.CharField(
        max_length=16,
        choices=ESTATUS,
        default=PENDIENTE
    )

    # choices
    CONTADO = 'AL CONTADO'
    ABONOS = 'EN ABONOS'
    FORMA_DE_PAGO = [
        (CONTADO, 'AL CONTADO'),
        (ABONOS, 'EN ABONOS')
    ]

    forma_de_pago = models.CharField(
        max_length=12,
        choices=FORMA_DE_PAGO,
        default=CONTADO
    )

    monto_recibido = models.IntegerField(null=True, blank=True)
    notas = models.CharField(max_length=250, blank=True, null=True)

    # llaves foráneas
    # un cliente solicita 1 o varias instalaciones
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE, default="")

    def __str__(self):
        """ Descripción del modelo instalación """
        return 'Se realizo el: {} y fue {}'.format(self.fecha, self.forma_de_pago)

    class Meta:
        """ nombre en plural(Muchos) del modelo"""
        verbose_name_plural = "Instalaciones"


class Contador(models.Model):
    """ modelo que representa un contador"""
    nombres = models.CharField(max_length=45, null=False, blank=False)
    ap_paterno = models.CharField(max_length=45, null=False, blank=False)
    ap_materno = models.CharField(max_length=45, null=True, blank=True)

    M = 'MASCULINO'
    F = 'FEMENINO'
    O = 'OTROS'
    GENERO = [
        (M, 'MASCULINO'),
        (F, 'FEMENINO'),
        (O, 'OTROS')
    ]
    genero = models.CharField(
        max_length=12,
        choices=GENERO,
        default=M
    )

    domicilio = models.CharField(max_length=100, null=True, blank=True)
    telefono = models.BigIntegerField(
        null=True,
        blank=True
    )
    correo = models.EmailField(null=True, blank=True)
    notas = models.CharField(max_length=100, null=True, blank=True)

    # llaves foráneas
    # un cliente pertenece a una localidad
    localidad = models.ForeignKey(
        Localidad, on_delete=models.CASCADE, default="")

    def __str__(self):
        """ Descripción del modelo contador """
        return '{} {} {}'.format(self.nombres, self.ap_paterno, self.ap_materno)

    def save(self):
        """ el nombre del contador lo guardamos en mayúsculas"""
        self.nombres = self.nombres.upper()
        if self.ap_paterno:  # si la cadena esta llena
            self.ap_paterno = self.ap_paterno.upper()

        if self.ap_materno:  # si la cadena esta llena
            self.ap_materno = self.ap_materno.upper()

        super(Contador, self).save()

    class Meta:
        """ nombre en plural(Muchos) del modelo"""
        verbose_name_plural = "Contadores"


class Pago(models.Model):
    """ modelo que representa un pago realizado"""
    fecha_de_emision = models.DateTimeField(
        auto_now_add=True, blank=False, null=False)

    M = 'MENSUALIDAD'
    AI = 'ABONO DE INSTALACIÓN'
    AMBOS = 'MENSUALIDAD Y ABONO DE INSTALACIÓN'
    CONCEPTO = [
        (M, 'MENSUALIDAD'),
        (AI, 'ABONO DE INSTALACIÓN'),
        (AMBOS, 'MENSUALIDAD Y ABONO DE INSTALACIÓN'),
    ]
    concepto = models.CharField(
        max_length=250,
        choices=CONCEPTO,
        default=M
    )

    total = models.IntegerField(null=True, blank=True)
    notas = models.CharField(max_length=150, blank=True, null=True)

    # llaves foráneas
    # un cliente realiza 1 o muchos pagos
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    # un contador recibe 1 o muchos pagos
    contador = models.ForeignKey(Contador, on_delete=models.CASCADE)

    def __str__(self):
        """ Descripción del modelo pago """
        return 'Pago de: {}, Total: {}'.format(self.concepto, self.total)

    class Meta:
        """ nombre en plural(Muchos) del modelo"""
        verbose_name_plural = "Pagos"
