"""
URL configuration for zanet project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from rest_framework.documentation import include_docs_urls
from django.conf.urls.static import static

urlpatterns = [
    path('', include(('bases.urls', 'bases'), namespace="bases")),
    path('', include(('app_clientes.urls', 'app_clientes'), namespace="app_clientes")),
    path('', include(('app_equipos.urls', 'app_equipos'), namespace="app_equipos")),
    path('', include(('app_perfiles.urls', 'app_perfiles'), namespace="app_perfiles")),
    path('', include(('app_publicidad.urls', 'app_publicidad'), namespace="app_publicidad")),
    path('admin/', admin.site.urls),
    path('api/v1/', include('api.urls')),
    path('docs/', include_docs_urls(title='API Documentación')),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
