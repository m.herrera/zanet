from django.shortcuts import render
from rest_framework import viewsets
from api.serializer import LocalidadSerializer, ClienteSerializer, InstalacionSerializer, ContadorSerializer, PagoSerializer, \
    ModemSerializer, EmisoaraSerializer, AntenaSerializer
from app_clientes.models import Localidad, Cliente, Instalacion, Contador, Pago
from app_equipos.models import Modem, Emisora, Antena

"""
Views de App Clientes
/////////////////////////////////////////////////////////////////////////////////
"""
# Create your views here.
class LocalidadsViewSet(viewsets.ModelViewSet):
    """ define el comportamiento de la vista"""
    queryset = Localidad.objects.all()
    serializer_class = LocalidadSerializer
    
class ClienteViewSet(viewsets.ModelViewSet):
    """ consultamos todos los registros de clientes y los serializamos"""
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer
    
class InstalacionViewSet(viewsets.ModelViewSet):    
    queryset = Instalacion.objects.all()
    serializer_class = InstalacionSerializer
    
class ContadorViewSet(viewsets.ModelViewSet):    
    queryset = Contador.objects.all()
    serializer_class = ContadorSerializer

class PagoViewSet(viewsets.ModelViewSet):
    queryset = Pago.objects.all()
    serializer_class = PagoSerializer
    
"""
Views de App Clientes
/////////////////////////////////////////////////////////////////////////////////
"""
class ModemViewSet(viewsets.ModelViewSet):
    queryset = Modem.objects.all()
    serializer_class = ModemSerializer
    
class EmisoraViewSet(viewsets.ModelViewSet):
    queryset = Emisora.objects.all()
    serializer_class = EmisoaraSerializer
    
class AntenaViewSet(viewsets.ModelViewSet):
    queryset = Antena.objects.all()
    serializer_class = AntenaSerializer