from django.urls import path, include
from rest_framework import routers
from api import views

# Routers - provee una manera facil de redirigir a los endpoint
# metodos accecibles GET, POST, PUT, DELETE
router = routers.DefaultRouter()

# rutas de app clientes
router.register(r'localidades', views.LocalidadsViewSet)
router.register(r'clientes', views.ClienteViewSet)
router.register(r'instalaciones', views.InstalacionViewSet)
router.register(r'contadores', views.ContadorViewSet)
router.register(r'pagos', views.PagoViewSet)

# rutas de app equipos
router.register(r'modems', views.ModemViewSet)
router.register(r'emisoras', views.EmisoraViewSet)
router.register(r'antenas', views.AntenaViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]