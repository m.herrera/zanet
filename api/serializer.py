from rest_framework import serializers
from app_clientes.models import Localidad, Cliente, Instalacion, Contador, Pago
from app_equipos.models import Modem, Emisora, Antena

"""
Serializers de App Clientes
/////////////////////////////////////////////////////////////////////////////////
"""

class LocalidadSerializer(serializers.ModelSerializer):
    """ define la representacion de la API - transforma los datos del modelo a json/xml"""
    class Meta:
        model = Localidad
        fields = '__all__'
        
class ClienteSerializer(serializers.ModelSerializer):
    """ define la representacion de la API - transforma los datos del modelo a json/xml"""
    class Meta:
        model = Cliente
        fields = '__all__'
        
class InstalacionSerializer(serializers.ModelSerializer):
    """ transforma los datos del modelo/tabla instalacion a json/xml"""
    class Meta:
        model = Instalacion
        fields = '__all__'
        
class ContadorSerializer(serializers.ModelSerializer):
    """ transforma los datos del modelo/tabla cliente a json/xml"""
    class Meta:
        model = Contador
        fields = '__all__'
        
class PagoSerializer(serializers.ModelSerializer):
    """ transforma los datos del modelo/tabla pago a json/xml"""
    class Meta:
        model = Pago
        fields = '__all__'
        
"""
Serializers de App Equipos
/////////////////////////////////////////////////////////////////////////////////
"""
class ModemSerializer(serializers.ModelSerializer):
    """ transforma los datos del modelo/tabla Modem a json/xml"""
    class Meta:
        model = Modem
        fields = '__all__'
        
class EmisoaraSerializer(serializers.ModelSerializer):
    """ transforma los datos del modelo/tabla Emisora a json/xml"""
    class Meta:
        model = Emisora
        fields = '__all__'
        
class AntenaSerializer(serializers.ModelSerializer):
    """ transforma los datos del modelo/tabla Antena a json/xml"""
    class Meta:
        model = Antena
        fields = '__all__'
    